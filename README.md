# Tutorials

Some tutorials I'm working on in the hope that they will benefit others.

Find them in the [Wiki](https://gitlab.com/johanmcos/tutorials/-/wikis/home)

Copyright (C) 2020 Johan M. Cos

These tutorials are distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Follow them at your own risk, do your own research, test them on a VM first, and always have good backups. I cannot and will not be held liable for any damage that following these tutorials might cause.

See the LICENSE file for more details.
